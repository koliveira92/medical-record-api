import { Includeable } from 'sequelize/types';
import Schedule from '../../../app/models/schedule.model';
import { IScheduleRepository } from '../../../app/repositories/schedule.repository';

export class ScheduleRepositoryMock implements IScheduleRepository {

  private includeScope: Includeable[] = [];

  private schedules: any[] = [
    {
      id: 'be76a874-790e-4ab8-8b94-0b7fdbd800bb',
      patientId: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
      dateStart: new Date('2021-11-16T08:00:00.000Z'),
      dateEnd: new Date('2021-11-16T08:30:00.000Z')
    },
    {
      id: '8ce95dce-b45a-4667-a026-22eadcab7871',
      patientId: '6243a999-8c0d-45b4-a95f-b723eef4b069',
      dateStart: new Date('2021-11-16T10:00:00.000Z'),
      dateEnd: new Date('2021-11-16T10:30:00.000Z')
    }
  ];

  async getAll(): Promise<Schedule[]> {
    return Promise.resolve(this.schedules);
  }

  async getAllByStartAndEndDate(start: Date, end: Date): Promise<Schedule[]> {
    return Promise.resolve(this.schedules.filter(s => s.dateStart.valueOf() === new Date(start).valueOf()));
  }

  async getById(id: string): Promise<Schedule | null> {
    return Promise.resolve(this.schedules.find(s => s.id === id));
  }

  async create(data: Schedule): Promise<Schedule | null> {
    data.id = 'new-uuid';
    return Promise.resolve(data);
  }

  async update(id: string, data: Schedule): Promise<Schedule | null> {
    data.id = id;
    return Promise.resolve(data);
  }

  async deleteById(id: string): Promise<void> {
    return Promise.resolve();
  }
}
