import Patient from '../../../app/models/patient.model';
import { IPatientRepository } from '../../../app/repositories/patient.repository';

export class PatientRepositoryMock implements IPatientRepository {

  private patients: any[] = [
    {
      id: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
      name: 'Patient Test 1',
      phone: '(17) 912345678',
      email: 'patient.test1@outlook.com',
      birthDate: new Date('1990-11-07T00:00:00.000Z'),
      sex: 'M',
      height: 1.8,
      weight: 80
    },
    {
      id: '6243a999-8c0d-45b4-a95f-b723eef4b069',
      name: 'Patient Test 2',
      phone: '(17) 912345678',
      email: 'patient.test2@outlook.com',
      birthDate: new Date('1990-11-07T00:00:00.000Z'),
      sex: 'F',
      height: 1.6,
      weight: 60
    }
  ];

  async getAll(): Promise<Patient[]> {
    return Promise.resolve(this.patients);
  }

  async getById(id: string): Promise<Patient | null> {
    return Promise.resolve(this.patients.find(p => p.id === id));
  }

  async create(data: Patient): Promise<Patient | null> {
    data.id = 'new-uuid';
    return Promise.resolve(data);
  }

  async update(id: string, data: Patient): Promise<Patient | null> {
    data.id = id;
    return Promise.resolve(data);
  }

  async deleteById(id: string): Promise<void> {
    return Promise.resolve();
  }
}
