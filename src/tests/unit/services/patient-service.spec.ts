import { PatientNotFoundError } from '../../../app/errors/patient.error';
import { PatientService } from '../../../app/services/patient.service';
import { PatientRepositoryMock } from '../../mocks/repositories/patient-repository.mock';

describe('PatientService', () => {

  const patientRepositoryMock = new PatientRepositoryMock();
  const patientService = new PatientService(patientRepositoryMock);

  beforeAll(async () => {

  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  describe('getAll', () => {
    it('should get all patients', async () => {
      const result = await patientService.getAll();

      expect(result).toBeInstanceOf(Array);
      expect(result.length).toBe(2);
      expect(result[0].id).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
      expect(result[1].id).toBe('6243a999-8c0d-45b4-a95f-b723eef4b069');
    });
  });

  describe('getById', () => {
    it('should get a patient by id', async () => {
      const result = await patientService.getById('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');

      expect(result).toBeDefined();
      expect(result!.id).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
      expect(result!.name).toBe('Patient Test 1');
    });

    it('should throw an error when a patient not exists', async () => {
      try {
        await patientService.getById('not-found-uuid');

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(PatientNotFoundError);
      }
    });
  });

  describe('create', () => {
    it('should create a patient', async () => {
      const patient: any = {
        name: 'Patient Test 3',
        phone: '(17) 912345678',
        email: 'patient.test3@outlook.com',
        birthDate: '1990-11-07T00:00:00.000Z',
        sex: 'M',
        height: 1.7,
        weight: 70
      };

      const result = await patientService.create(patient);

      expect(result).toBeDefined();
      expect(result!.id).toBe('new-uuid');
      expect(result!.name).toBe('Patient Test 3');
    });
  });

  describe('update', () => {
    it('should update a patient', async () => {
      const patient: any = {
        name: 'Patient Test 1 UPDATED',
        phone: '(17) 91234567',
        email: 'patient.test3@outlook.com',
        birthDate: '1990-11-07T00:00:00.000Z',
        sex: 'M',
        height: 1.7,
        weight: 70
      };

      const result = await patientService.update('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a', patient);

      expect(result).toBeDefined();
      expect(result!.id).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
      expect(result!.name).toBe('Patient Test 1 UPDATED');
      expect(result!.phone).toBe('(17) 91234567');
    });

    it('should throw an error when a patient not exists', async () => {
      try {
        const patient: any = {
          name: 'Patient Test 1 UPDATED',
          phone: '(17) 91234567',
          email: 'patient.test3@outlook.com',
          birthDate: '1990-11-07T00:00:00.000Z',
          sex: 'M',
          height: 1.7,
          weight: 70
        };

        const result = await patientService.update('not-found-uuid', patient);

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(PatientNotFoundError);
      }
    });
  });

  describe('deleteById', () => {
    it('should delete a patient', async () => {
      const result = await patientService.deleteById('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');

      expect(result).toBeUndefined();
    });

    it('should throw an error when a patient not exists', async () => {
      try {
        await patientService.getById('not-found-uuid');

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(PatientNotFoundError);
      }
    });
  });
});
