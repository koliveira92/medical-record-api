import { ScheduleNotFoundError, ScheduleWithConflitedDateError } from '../../../app/errors/schedule.error';
import { ScheduleService } from '../../../app/services/schedule.service';
import { ScheduleRepositoryMock } from '../../mocks/repositories/schedule-repository.mock';

describe('ScheduleService', () => {

  const scheduleRepositoryMock = new ScheduleRepositoryMock();
  const scheduleService = new ScheduleService(scheduleRepositoryMock);

  beforeAll(async () => {

  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  describe('getAll', () => {
    it('should get all schedules', async () => {
      const result = await scheduleService.getAll();

      expect(result).toBeInstanceOf(Array);
      expect(result.length).toBe(2);
      expect(result[0].id).toBe('be76a874-790e-4ab8-8b94-0b7fdbd800bb');
      expect(result[1].id).toBe('8ce95dce-b45a-4667-a026-22eadcab7871');
    });
  });

  describe('getById', () => {
    it('should get a schedule by id', async () => {
      const result = await scheduleService.getById('be76a874-790e-4ab8-8b94-0b7fdbd800bb');

      expect(result).toBeDefined();
      expect(result!.id).toBe('be76a874-790e-4ab8-8b94-0b7fdbd800bb');
      expect(result!.patientId).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
    });

    it('should throw an error when a schedule not exists', async () => {
      try {
        await scheduleService.getById('not-found-uuid');

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(ScheduleNotFoundError);
      }
    });
  });

  describe('create', () => {
    it('should create a schedule', async () => {
      const schedule: any = {
        patientId: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
        dateStart: '2021-11-16T12:00:00.000Z',
        dateEnd: '2021-11-16T12:30:00.000Z'
      };

      const result = await scheduleService.create(schedule);

      expect(result).toBeDefined();
      expect(result!.id).toBe('new-uuid');
      expect(result!.patientId).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
    });

    it('should throw an error when a schedule have conflited dates', async () => {
      try {
        const schedule: any = {
          patientId: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
          dateStart: '2021-11-16T08:00:00.000Z',
          dateEnd: '2021-11-16T08:30:00.000Z'
        };

        await scheduleService.create(schedule);

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(ScheduleWithConflitedDateError);
      }
    });
  });

  describe('update', () => {
    it('should update a schedule', async () => {
      const schedule: any = {
        patientId: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
        dateStart: '2021-11-16T18:00:00.000Z',
        dateEnd: '2021-11-16T18:30:00.000Z'
      };

      const result = await scheduleService.update('be76a874-790e-4ab8-8b94-0b7fdbd800bb', schedule);

      expect(result).toBeDefined();
      expect(result!.id).toBe('be76a874-790e-4ab8-8b94-0b7fdbd800bb');
      expect(result!.patientId).toBe('cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a');
      expect(result!.dateStart).toBe('2021-11-16T18:00:00.000Z');
      expect(result!.dateEnd).toBe('2021-11-16T18:30:00.000Z');
    });

    it('should throw an error when a schedule not exists', async () => {
      try {
        const schedule: any = {
          patientId: 'cc8a2b1f-9d2e-4d80-9fbb-59bac90a2d8a',
          dateStart: '2021-11-16T18:00:00.000Z',
          dateEnd: '2021-11-16T18:30:00.000Z'
        };

        await scheduleService.update('not-found-uuid', schedule);

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(ScheduleNotFoundError);
      }
    });
  });

  describe('deleteById', () => {
    it('should delete a schedule', async () => {
      const result = await scheduleService.deleteById('be76a874-790e-4ab8-8b94-0b7fdbd800bb');

      expect(result).toBeUndefined();
    });

    it('should throw an error when a schedule not exists', async () => {
      try {
        await scheduleService.getById('not-found-uuid');

        expect(true).toBe(false);

      } catch (err) {
        expect(err).toBeInstanceOf(ScheduleNotFoundError);
      }
    });
  });
});
