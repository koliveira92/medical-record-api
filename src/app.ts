import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import { Database } from './database/sequelize';
import errorHandler from './app/middlewares/error-handler.middleware';
import { IndexRouter } from './app/routers/index.router';

dotenv.config();

class App {

  private app: express.Application;

  constructor() {
    this.app = express();

    this.connectToTheDatabase();
    this.registerBeforeMiddlewares();

    this.app.use('/v1/', new IndexRouter().getRouter());

    this.registerAfterMiddlewares();
  }

  listen() {
    const port = process.env.PORT;

    this.app.listen(port, () => {
      console.log(`App listening on the port ${port}`);
    });
  }

  private connectToTheDatabase() {
    const database = new Database();
    database.init();
  }

  private registerBeforeMiddlewares() {
    this.app.use(cors());
    this.app.use(express.json());

    console.log('Before Middlewares have been registered');
  }

  private registerAfterMiddlewares() {
    this.app.use(errorHandler());

    console.log('After Middlewares have been registered');
  }
}

const app = new App();
app.listen();
