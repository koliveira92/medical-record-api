'use strict';

const TABLE_NAME = 'patients';
const COLUMN_NAME = 'deletedAt';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(TABLE_NAME, COLUMN_NAME, {
      type: Sequelize.DATE,
      allowNull: true,
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(TABLE_NAME, COLUMN_NAME);
  }
};
