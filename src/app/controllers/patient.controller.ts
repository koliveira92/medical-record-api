import { NextFunction, Request, Response } from 'express';
import PatientNote from '../models/patient-note.model';
import Patient from '../models/patient.model';
import { IPatientService, PatientService } from '../services/patient.service';
import { BaseController } from './base.controller';

export class PatientController extends BaseController<Patient> {

  service: IPatientService;

  constructor(service = new PatientService()) {
    super();
    this.service = service;
  }

  async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.getAll();
      this.ok<Patient[]>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async get(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.getById(req.params.id);
      this.ok<Patient | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async getNotes(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.getNotes(req.params.id);
      this.ok<PatientNote[]>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async post(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.create(req.body);
      this.created<Patient | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async postNote(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.createNote(req.params.id, req.body);
      this.created<PatientNote | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async put(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.update(req.params.id, req.body);
      this.ok<Patient | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      await this.service.deleteById(req.params.id);
      this.noContent(res);
    } catch (err) {
      next(err);
    }
  }
}
