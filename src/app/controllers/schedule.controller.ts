import { NextFunction, Request, Response } from 'express';
import Schedule from '../models/schedule.model';
import { IScheduleService, ScheduleService } from '../services/schedule.service';
import { BaseController } from './base.controller';

export class ScheduleController extends BaseController<Schedule> {

  service: IScheduleService;

  constructor(service = new ScheduleService()) {
    super();
    this.service = service;
  }

  async getAll(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.getAll();
      this.ok<Schedule[]>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async get(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.getById(req.params.id);
      this.ok<Schedule | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async post(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.create(req.body);
      this.created<Schedule | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async put(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await this.service.update(req.params.id, req.body);
      this.ok<Schedule | null>(res, data);
    } catch (err) {
      next(err);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      await this.service.deleteById(req.params.id);
      this.noContent(res);
    } catch (err) {
      next(err);
    }
  }
}
