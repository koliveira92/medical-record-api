import { Response } from 'express';
import { IService } from '../services/base.service';

export abstract class BaseController<T> {

  service: IService<T>;

  ok<T>(res: Response, data?: T) {
    if (!!data) {
      return res.status(200).json(data);
    } else {
      return res.sendStatus(200);
    }
  }

  created<T>(res: Response, data?: T) {
    if (!!data) {
      return res.status(201).json(data);
    } else {
      return res.sendStatus(201);
    }
  }

  noContent(res: Response) {
    return res.sendStatus(204);
  }
}
