import { BaseError } from './base.error';

export class ScheduleNotFoundError extends BaseError<void> {
  statusCode = 404;
  message = 'Schedule not found';
}

export class ScheduleWithConflitedDateError extends BaseError<void> {
  statusCode = 400;
  message = 'Schedule with conflited dates';
}
