import { BaseError } from './base.error';

export class ValidationError extends BaseError<string[]> {

  statusCode = 400;
  message = 'Invalid data';

  constructor(errors: string[]) {
    super();
    this.additionalData = errors;
  }
}
