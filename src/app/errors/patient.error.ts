import { BaseError } from './base.error';

export class PatientNotFoundError extends BaseError<void> {
  statusCode = 404;
  message = 'Patient not found';
}
