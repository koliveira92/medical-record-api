export abstract class BaseError<T> extends Error {
  statusCode: number;
  message: string;
  additionalData: T;
}
