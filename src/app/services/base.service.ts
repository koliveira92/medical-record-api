import { IRepository } from '../repositories/base.repository';

export interface IService<T> {
  repository: IRepository<T>;
}
