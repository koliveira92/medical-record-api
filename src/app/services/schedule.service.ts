import { ValidationError } from '../errors/generic.error';
import { ScheduleNotFoundError, ScheduleWithConflitedDateError } from '../errors/schedule.error';
import Schedule from '../models/schedule.model';
import { IScheduleRepository, ScheduleRepository } from '../repositories/schedule.repository';
import { ScheduleValidator } from '../validators/schedule.validator';
import { IService } from './base.service';

export interface IScheduleService extends IService<Schedule> {
  repository: IScheduleRepository;
  getAll(): Promise<Schedule[]>;
  getById(id: string): Promise<Schedule | null>;
  create(data: Schedule): Promise<Schedule | null>;
  update(id: string, data: Schedule): Promise<Schedule | null>;
  deleteById(id: string): Promise<void>;
}

export class ScheduleService implements IScheduleService {

  repository: IScheduleRepository;

  constructor(
    repository: IScheduleRepository = new ScheduleRepository()) {

    this.repository = repository;
  }

  async getAll(): Promise<Schedule[]> {
    return this.repository.getAll();
  }

  async getById(id: string): Promise<Schedule | null> {
    const schedule = await this.repository.getById(id);
    if (!schedule) {
      throw new ScheduleNotFoundError();
    }

    return schedule;
  }

  async create(data: Schedule): Promise<Schedule | null> {
    const scheduleValidator = new ScheduleValidator();
    const errors = scheduleValidator.validate(data);
    if (errors && errors.length > 0) {
      throw new ValidationError(errors);
    }

    const conflitedSchedules = await this.repository.getAllByStartAndEndDate(data.dateStart, data.dateEnd);
    if (conflitedSchedules && conflitedSchedules.length > 0) {
      throw new ScheduleWithConflitedDateError();
    }

    return this.repository.create(data);
  }

  async update(id: string, data: Schedule): Promise<Schedule | null> {
    const schedule = await this.repository.getById(id);
    if (!schedule) {
      throw new ScheduleNotFoundError();
    }

    const scheduleValidator = new ScheduleValidator();
    const errors = scheduleValidator.validate(data);
    if (errors && errors.length > 0) {
      throw new ValidationError(errors);
    }

    return this.repository.update(id, data);
  }

  async deleteById(id: string): Promise<void> {
    const schedule = await this.repository.getById(id);
    if (!schedule) {
      throw new ScheduleNotFoundError();
    }

    return this.repository.deleteById(id);
  }
}
