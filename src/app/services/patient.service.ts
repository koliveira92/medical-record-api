import { ValidationError } from '../errors/generic.error';
import { PatientNotFoundError } from '../errors/patient.error';
import Patient from '../models/patient.model';
import PatientNote from '../models/patient-note.model';
import { IPatientRepository, PatientRepository } from '../repositories/patient.repository';
import { PatientValidator } from '../validators/patient.validator';
import { IService } from './base.service';
import { IPatientNoteService, PatientNoteService } from './patient-note.service';

export interface IPatientService extends IService<Patient> {
  repository: IPatientRepository;
  patientNoteService: IPatientNoteService;
  getAll(): Promise<Patient[]>;
  getById(id: string): Promise<Patient | null>;
  getNotes(id: string): Promise<PatientNote[]>;
  create(data: Patient): Promise<Patient | null>;
  createNote(id: string, data: PatientNote): Promise<PatientNote | null>;
  update(id: string, data: Patient): Promise<Patient | null>;
  deleteById(id: string): Promise<void>;
}

export class PatientService implements IPatientService {

  repository: IPatientRepository;
  patientNoteService: IPatientNoteService;

  constructor(
    repository: IPatientRepository = new PatientRepository(),
    patientNoteService: IPatientNoteService = new PatientNoteService()) {

    this.repository = repository;
    this.patientNoteService = patientNoteService;
  }

  async getAll(): Promise<Patient[]> {
    return this.repository.getAll();
  }

  async getById(id: string): Promise<Patient | null> {
    const patient = await this.repository.getById(id);
    if (!patient) {
      throw new PatientNotFoundError();
    }

    return patient;
  }

  async getNotes(id: string): Promise<PatientNote[]> {
    const patient = await this.repository.getById(id);
    if (!patient) {
      throw new PatientNotFoundError();
    }

    return this.patientNoteService.getByAllPatientId(patient.id);
  }

  async create(data: Patient): Promise<Patient | null> {
    const patientValidator = new PatientValidator();
    const errors = patientValidator.validate(data);
    if (errors && errors.length > 0) {
      throw new ValidationError(errors);
    }

    return this.repository.create(data);
  }

  async createNote(id: string, data: PatientNote): Promise<PatientNote | null> {
    const patient = await this.repository.getById(id);
    if (!patient) {
      throw new PatientNotFoundError();
    }

    data.patientId = patient.id;

    return this.patientNoteService.create(data);
  }

  async update(id: string, data: Patient): Promise<Patient | null> {
    const patient = await this.repository.getById(id);
    if (!patient) {
      throw new PatientNotFoundError();
    }

    const patientValidator = new PatientValidator();
    const errors = patientValidator.validate(data);
    if (errors && errors.length > 0) {
      throw new ValidationError(errors);
    }

    return this.repository.update(id, data);
  }

  async deleteById(id: string): Promise<void> {
    const patient = await this.repository.getById(id);
    if (!patient) {
      throw new PatientNotFoundError();
    }

    const newPatient: any = {
      name: 'Removed for LGPD reasons',
      phone: null,
      email: null,
      birthDate: null,
      sex: null,
      height: null,
      weight: null,
    };

    await this.repository.update(id, newPatient);

    // It is a soft delete
    return this.repository.deleteById(id);
  }
}
