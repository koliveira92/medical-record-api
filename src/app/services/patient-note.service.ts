import { ValidationError } from '../errors/generic.error';
import PatientNote from '../models/patient-note.model';
import { IPatientNoteRepository, PatientNoteRepository } from '../repositories/patient-note.repository';
import { PatientNoteValidator } from '../validators/patient-note.validator';
import { IService } from './base.service';

export interface IPatientNoteService extends IService<PatientNote> {
  repository: IPatientNoteRepository;
  getByAllPatientId(patientId: string): Promise<PatientNote[]>;
  create(data: PatientNote): Promise<PatientNote | null>;
}

export class PatientNoteService implements IPatientNoteService {

  repository: IPatientNoteRepository;

  constructor(repository = new PatientNoteRepository()) {
    this.repository = repository;
  }

  async getByAllPatientId(patientId: string): Promise<PatientNote[]> {
    return this.repository.getByAllPatientId(patientId);
  }

  async create(data: PatientNote): Promise<PatientNote | null> {
    const patientNoteValidator = new PatientNoteValidator();
    const errors = patientNoteValidator.validate(data);
    if (errors && errors.length > 0) {
      throw new ValidationError(errors);
    }

    return this.repository.create(data);
  }
}
