import Patient from '../models/patient.model';
import { IRepository } from './base.repository';

export interface IPatientRepository extends IRepository<Patient> {
  getAll(): Promise<Patient[]>;
  getById(id: string): Promise<Patient | null>;
  create(data: Patient): Promise<Patient | null>;
  update(id: string, data: Patient): Promise<Patient | null>;
  deleteById(id: string): Promise<void>;
}

export class PatientRepository implements IPatientRepository {

  async getAll(): Promise<Patient[]> {
    return await Patient.findAll();
  }

  async getById(id: string): Promise<Patient | null> {
    return await Patient.findByPk(id);
  }

  async create(data: Patient): Promise<Patient | null> {
    const patient = await Patient.create(data);
    return this.getById(patient.id);
  }

  async update(id: string, data: Patient): Promise<Patient | null> {
    await Patient.update(data, { where: { id: id } });
    return this.getById(id);
  }

  async deleteById(id: string): Promise<void> {
    await Patient.destroy({ where: { id: id } });
    return;
  }
}
