import { Op } from 'sequelize'
import { Includeable } from 'sequelize/types';
import Patient from '../models/patient.model';
import Schedule from '../models/schedule.model';
import { IRepository } from './base.repository';

export interface IScheduleRepository extends IRepository<Schedule> {
  getAll(): Promise<Schedule[]>;
  getAllByStartAndEndDate(start: Date, end: Date): Promise<Schedule[]>;
  getById(id: string): Promise<Schedule | null>;
  create(data: Schedule): Promise<Schedule | null>;
  update(id: string, data: Schedule): Promise<Schedule | null>;
  deleteById(id: string): Promise<void>;
}

export class ScheduleRepository implements IScheduleRepository {

  private includeScope: Includeable[] = [
    { model: Patient }
  ];

  async getAll(): Promise<Schedule[]> {
    return await Schedule.findAll({ include: this.includeScope });
  }

  async getAllByStartAndEndDate(start: Date, end: Date): Promise<Schedule[]> {
    return await Schedule.findAll({
      where: {
        [Op.or]: [
          {
            [Op.and]: [
              { dateStart: { [Op.gt]: start } },
              { dateStart: { [Op.lt]: end } }
            ]
          },
          {
            [Op.and]: [
              { dateEnd: { [Op.gt]: start } },
              { dateEnd: { [Op.lt]: end } }
            ]
          },
          {
            [Op.and]: [
              { dateStart: { [Op.lte]: start } },
              { dateEnd: { [Op.gte]: end } }
            ]
          }
        ]
      },
      include: this.includeScope
    });
  }

  async getById(id: string): Promise<Schedule | null> {
    return await Schedule.findByPk(id, { include: this.includeScope });
  }

  async create(data: Schedule): Promise<Schedule | null> {
    const schedule = await Schedule.create(data);
    return this.getById(schedule.id);
  }

  async update(id: string, data: Schedule): Promise<Schedule | null> {
    await Schedule.update(data, { where: { id: id } });
    return this.getById(id);
  }

  async deleteById(id: string): Promise<void> {
    await Schedule.destroy({ where: { id: id } });
    return;
  }
}
