import PatientNote from '../models/patient-note.model';
import { IRepository } from './base.repository';

export interface IPatientNoteRepository extends IRepository<PatientNote> {
  getByAllPatientId(patientId: string): Promise<PatientNote[]>;
  getById(id: string): Promise<PatientNote | null>;
  create(data: PatientNote): Promise<PatientNote | null>;
}

export class PatientNoteRepository implements IPatientNoteRepository {

  async getByAllPatientId(patientId: string): Promise<PatientNote[]> {
    return await PatientNote.findAll({ where: { patientId: patientId } });
  }

  async getById(id: string): Promise<PatientNote | null> {
    return await PatientNote.findByPk(id);
  }

  async create(data: PatientNote): Promise<PatientNote | null> {
    const patientNote = await PatientNote.create(data);
    return this.getById(patientNote.id);
  }
}
