import { Column, DataType, Default, PrimaryKey, Table, Model, AllowNull } from 'sequelize-typescript'

@Table({
  tableName: 'patients',
  timestamps: true,
  paranoid: true,
})
export default class Patient extends Model {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  id: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  phone: string;

  @Column(DataType.STRING)
  email: string;

  @Column(DataType.DATE)
  birthDate: Date;

  @Column(DataType.ENUM('M', 'F'))
  sex: string;

  @Column(DataType.DECIMAL)
  height: number;

  @Column(DataType.DECIMAL)
  weight: number;
}
