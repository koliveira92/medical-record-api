import { Column, DataType, Default, ForeignKey, HasOne, PrimaryKey, Table, Model, AllowNull } from 'sequelize-typescript'
import Patient from './patient.model';

@Table({
  tableName: 'schedules',
  timestamps: true,
  paranoid: false,
})
export default class Schedule extends Model {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  id: string;

  @ForeignKey(() => Patient)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  patientId: string;

  @HasOne(() => Patient, {
    sourceKey: 'patientId',
    foreignKey: 'id',
  })
  patient: Patient;

  @AllowNull(false)
  @Column(DataType.DATE)
  dateStart: Date;

  @AllowNull(false)
  @Column(DataType.DATE)
  dateEnd: Date;
}
