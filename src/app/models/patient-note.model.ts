import { Column, DataType, Default, ForeignKey, PrimaryKey, Table, Model, AllowNull } from 'sequelize-typescript'
import Patient from './patient.model';

@Table({
  tableName: 'patient_notes',
  timestamps: true,
  paranoid: false,
})
export default class PatientNote extends Model {
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  id: string;

  @ForeignKey(() => Patient)
  @AllowNull(false)
  @Column(DataType.UUIDV4)
  patientId: string;

  @AllowNull(false)
  @Column(DataType.DATE)
  date: Date;

  @AllowNull(false)
  @Column(DataType.STRING)
  note: string;
}
