import { Request, Response, NextFunction } from 'express';
import { BaseError } from '../errors/base.error';

export default () => {
  return (err: Error, req: Request, res: Response, next: NextFunction) => {
    console.error(err);

    if (err instanceof BaseError) {
      const code = err.statusCode || 500;
      const message = err.message || 'An unexpected error occurred';
      const additionalData = err.additionalData;

      if (additionalData) {
        res.status(code).json({ code, message, additionalData });
      } else {
        res.status(code).json({ code, message });
      }
    }

    next();
  }
}
