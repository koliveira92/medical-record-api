import Joi from 'joi';
import Schedule from '../models/schedule.model';
import { BaseValidator } from './base.validator';

export class ScheduleValidator extends BaseValidator<Schedule> {

  constructor() {
    super();

    const patientId = Joi.string()
      .uuid()
      .required();

    const dateStart = Joi.string()
      .isoDate()
      .required();

    const dateEnd = Joi.string()
      .isoDate()
      .required();

    this.validator = Joi.object({
      patientId,
      dateStart,
      dateEnd
    });
  }
}
