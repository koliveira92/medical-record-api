import Joi from 'joi';
import PatientNote from '../models/patient-note.model';
import { BaseValidator } from './base.validator';

export class PatientNoteValidator extends BaseValidator<PatientNote> {

  constructor() {
    super();

    const patientId = Joi.string()
      .uuid()
      .required();

    const date = Joi.string()
      .isoDate()
      .required();

    const note = Joi.string()
      .required();

    this.validator = Joi.object({
      patientId,
      date,
      note
    });
  }
}
