import Joi from 'joi';
import Patient from '../models/patient.model';
import { BaseValidator } from './base.validator';

export class PatientValidator extends BaseValidator<Patient> {

  constructor() {
    super();

    const name = Joi.string()
      .required();

    const phone = Joi.string()
      .pattern(/\(\d{2}\)\s\d{8,9}/)
      .messages({
        'string.pattern.base': '\"phone\" must be a valid phone: \"(##) ########\" or \"(##) #########\"'
      });

    const email = Joi.string()
      .email();

    const birthDate = Joi.string()
      .isoDate();

    const sex = Joi.string()
      .valid('M', 'F');

    const height = Joi.number()
      .precision(2);

    const weight = Joi.number()
      .integer();

    this.validator = Joi.object({
      name,
      phone,
      email,
      birthDate,
      sex,
      height,
      weight
    });
  }
}
