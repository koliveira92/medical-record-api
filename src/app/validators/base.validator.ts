import Joi from 'joi';

export abstract class BaseValidator<T> {

  protected validator: Joi.ObjectSchema;

  validate(data: T): string[] | undefined {
    const options = { abortEarly: false, convert: false };
    const { error } = this.validator.validate(data, options);

    if (error) {
      return error.details.map(x => x.message);
    }

    return;
  }
}
