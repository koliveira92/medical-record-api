import { ScheduleController } from '../controllers/schedule.controller';
import { BaseRouter } from './base.router';

export class ScheduleRouter extends BaseRouter {

  constructor(controller = new ScheduleController()) {
    super();

    this.router.get('/', (req, res, next) => controller.getAll(req, res, next));
    this.router.get('/:id', (req, res, next) => controller.get(req, res, next));
    this.router.post('/', (req, res, next) => controller.post(req, res, next));
    this.router.put('/:id', (req, res, next) => controller.put(req, res, next));
    this.router.delete('/:id', (req, res, next) => controller.delete(req, res, next));
  }
}
