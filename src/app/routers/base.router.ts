import express, { Router } from 'express';

export abstract class BaseRouter {

  protected router = express.Router();

  getRouter(): Router {
    return this.router;
  }
}
