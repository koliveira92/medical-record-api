import { BaseRouter } from './base.router';
import { PatientRouter } from './patient.router';
import { ScheduleRouter } from './schedule.router';

export class IndexRouter extends BaseRouter {

  constructor() {
    super();

    this.router.use('/patient', new PatientRouter().getRouter());
    this.router.use('/schedule', new ScheduleRouter().getRouter());
  }
}
