import { PatientController } from '../controllers/patient.controller';
import { BaseRouter } from './base.router';

export class PatientRouter extends BaseRouter {

  constructor(controller = new PatientController()) {
    super();

    this.router.get('/', (req, res, next) => controller.getAll(req, res, next));
    this.router.get('/:id', (req, res, next) => controller.get(req, res, next));
    this.router.get('/:id/notes', (req, res, next) => controller.getNotes(req, res, next));
    this.router.post('/', (req, res, next) => controller.post(req, res, next));
    this.router.post('/:id/notes', (req, res, next) => controller.postNote(req, res, next));
    this.router.put('/:id', (req, res, next) => controller.put(req, res, next));
    this.router.delete('/:id', (req, res, next) => controller.delete(req, res, next));
  }
}
