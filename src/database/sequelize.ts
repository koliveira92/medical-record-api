import { Sequelize } from 'sequelize-typescript'

export class Database {

  sequelize: Sequelize;

  init(): void {
    const databaseConfig = require('../config/sequelize');
    this.sequelize = new Sequelize(databaseConfig[process.env.NODE_ENV || 'development']);
    this.sequelize.addModels([__dirname + '/../app/models']);
  }
}
