# Medical Record API #

Backend project for a Medical Record application

## Setup and Installation

### Setup the Node.js

**(Important)** Make sure you have [NVM](https://github.com/nvm-sh/nvm) installed.

Install the version that is compatible with this project:

```sh
$ nvm use
```

Install Node.js dependencies

```sh
$ npm install
```

### Setup the database

**(Important)** Make sure you have [Docker](https://docs.docker.com/engine/install/) installed.

Start the database container:

```sh
$ docker-compose up -d
```

Create the database

```sh
$ npm run db-create
```

Run all migrations

```sh
$ npm run db-migrate
```

## Running locally

Run the project

```sh
$ npm run start
```

## Running unit tests

To execute the unit tests via Jest, run:

```sh
$ npm run unit-test
```

## API Documentation

**(Important)** Make sure you have [Postman](https://www.postman.com/) installed.

Import the file `/postman/Medical Record API.postman_collection.json` on Postman
